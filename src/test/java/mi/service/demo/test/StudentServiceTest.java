package mi.service.demo.test;

import java.util.Collection;

import javax.transaction.Transactional;

import mi.model.demo.Student;
import mi.service.demo.IStudentService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:springconf/applicationContext.xml"})
@Transactional
@TransactionConfiguration(transactionManager="transactionManager",defaultRollback=true)
//如果是true不会改变数据库数据，如果是false会改变数据
public class StudentServiceTest {

	/**
	 * 插入,修改,删除,冻结,查询
	 */
	
	@Autowired//根据类型注入
	private IStudentService studentService;
	
	@Test
	public void testInsert(){
		//1.创建对象数据模型
		Student student = new Student();
		student.setName("逗你呢");
		student.setAge(20);


		//2.使用业务方法执行插入
		studentService.insert(student);
		
		//3.使用Junit的Assert静态类中的一些方法判断ID是否大于0
		Assert.assertTrue(student.getId()>0);
		
		//(可选)4.测试插入数据的ID是多少 
		System.out.println("插入的新ID为："+student.getId());
	}
	
	@Test
	public void testDelete(){
		//1.创建对象数据模型
		Student student = new Student();
		student.setName("逗你呢");
		student.setAge(20);
		
		//2.使用业务方法执行插入
		studentService.insert(student);
		
		//3.使用Junit的Assert静态类中的一些方法判断ID是否大于0
		Assert.assertTrue(student.getId()>0);
		
		//4.保存一个临时ID值
		int _id = student.getId();
		
		//5.删除
		studentService.delete(student);
		
		//6.根据临时ID查询对象数据
		Student _student = studentService.find(new Student(_id));
		
		//7.断言数据是否已经删除成功
		Assert.assertTrue(_student==null);
	}
	
	@Test
	public void testUpdate(){
		//1.创建对象数据模型
		Student student = new Student();
		student.setName("逗你呢");
		student.setAge(20);
		
		//2.使用业务方法执行插入
		studentService.insert(student);
		
		//3.使用Junit的Assert静态类中的一些方法判断ID是否大于0
		Assert.assertTrue(student.getId()>0);
		
		//4.修改某些字段的数据
		student.setName("逗你呢");
		student.setAge(21);
		
		//5.执行修改方法 
		studentService.edit(student);
		
		//6.执行查询方法
		Student _student = studentService.find(student);
		
		//7.断言之前修改的字段数据与查询出来的字段数据是一至的
		Assert.assertTrue(_student.getName().equals("逗你呢")&&_student.getAge()==21);
		
		System.out.println(_student.getName());
	}
	
	@Test
	public void testSelect(){
		for(int i = 0;i<10;i++){
			//1.创建对象数据模型
			Student student = new Student();
			student.setName("逗你呢"+i);
			student.setAge(20);
			
			//2.使用业务方法执行插入
			studentService.insert(student);
			
			//3.使用Junit的Assert静态类中的一些方法判断ID是否大于0
			Assert.assertTrue(student.getId()>0);
		}
		
		//4.查询数据结果集
		Collection<Student> list = studentService.select(new Student());
		
		
		//5.断言数据结果集个数
		Assert.assertTrue(list.size()>10);
		
		for(Student student : list){
			System.out.println(student.getName());
		}
	}
}
