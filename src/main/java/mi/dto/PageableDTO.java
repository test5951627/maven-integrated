package mi.dto;

import mi.model.BaseModel;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:17:31 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 分页DTO ]
 */
@SuppressWarnings("unused")
public class PageableDTO extends BaseModel implements Pageable {
	private static final long serialVersionUID = -7364055872066873762L;

	/** 当前页数? */
	private Integer currentPage = 1;

	/** 每页显示记录数? */
	private Integer pageSize = 10;

	/** 总记录数 */
	private Integer recordCount = 0;

	/** 总页数 */
	private Integer pageCount = 0;

	/** 偏移一页 */
	private Integer offset = 0;

	/** 是否有上一页 */

	private boolean hasPrePage = false;

	/** 是否有下一页 */
	private boolean hasNextPage = false;

	@Override
	public int getPageNumber() {
		return currentPage;
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public int getOffset() {
		return offset;
	}

	@Override
	public Sort getSort() {
		return null;
	}

	@Override
	public Pageable next() {
		if (currentPage < pageCount) {// 当前页面小于总页面如进行下一页
			currentPage++;
			this.offset = (this.currentPage - 1) * pageSize;
		}

		hasPN();// 是否有上一页或者下一页

		return this;
	}

	public void hasPN() {
		if (currentPage < pageCount) {
			hasNextPage = true;
		} else {
			hasNextPage = false;
		}
		if (currentPage > 1) {
			hasPrePage = true;
		} else {
			hasPrePage = false;
		}
	}

	public Pageable prev() {
		if (currentPage > 1) {
			currentPage--;
			this.offset = (this.currentPage - 1) * pageSize;
		}

		this.hasPN();// 是否有上一页或者下一页

		return this;
	}

	@Override
	public Pageable previousOrFirst() {
		return null;
	}

	@Override
	public Pageable first() {
		this.currentPage = 1;
		this.offset = (this.currentPage - 1) * pageSize;
		return this;
	}

	@Override
	public boolean hasPrevious() {
		return false;
	}

	public void setRecordCount(Integer recordCount) {
		if (recordCount > 0) {
			this.recordCount = recordCount;
			this.pageCount = recordCount / pageSize;// 计算总页数
			this.offset = (this.currentPage - 1) * pageSize;// 计算偏移量
			this.hasPN();// 是否有上一页或者下一页
		} else {
			throw new RuntimeException("数据总数不能低于0");
		}
	}

	public void setCurrentPage(Integer currentPage) {
		if (currentPage > 0 && currentPage <= pageCount) {// 设置的页数必须大于0并且小于或总页数
			this.currentPage = currentPage;
			this.offset = (this.currentPage - 1) * pageSize;// 计算偏移量
			this.hasPN();// 是否有上一页或者下一页
		} else {
			throw new RuntimeException("当前页数不能低于1并且不能大于总页数");
		}
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

}
