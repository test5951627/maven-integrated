package mi.dto.demo;

import java.io.Serializable;

import mi.dto.BaseDTO;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月26日 上午8:54:41 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 学生DTO ]
 */
public class StudentDTO extends BaseDTO implements Serializable{
	
	private static final long serialVersionUID = 247936892690812665L;

}
