package mi.common.validator;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:16:37 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 统一json数据格式 ]
 */
public class JsonResult implements Serializable {

	private static final long serialVersionUID = 510416383852349941L;

	private Boolean success = Boolean.TRUE;
	private String msg="success";
	private Set<String> msgs = new HashSet<String>(0);
	
	public JsonResult() {
		super();
	}

	public JsonResult(Boolean success,String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Set<String> getMsgs() {
		return msgs;
	}

	public void setMsgs(Set<String> msgs) {
		this.msgs = msgs;
	}

}
