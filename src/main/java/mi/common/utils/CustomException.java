package mi.common.utils;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:15:15 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 自定义异常类 ]
 */
public class CustomException extends RuntimeException{
	private static final long serialVersionUID = 6794838795960651420L;

	public CustomException(){
		super();
	}

	public CustomException(String message){
		super(message);
	}

	public CustomException(Throwable throwable){
		super(throwable);
	}

	public CustomException(String message, Throwable throwable){
		super(message, throwable);
	}
}
