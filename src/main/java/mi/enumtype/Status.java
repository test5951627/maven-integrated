package mi.enumtype;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:17:59 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 状态 ]
 */
public enum Status {
	NORMAL(0)//正常
	, CONGEAL(1)//冻结
	, THAW(2)//解冻 
	, DELETE(3)//删除
	;

	private int value;

	private Status(final int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
