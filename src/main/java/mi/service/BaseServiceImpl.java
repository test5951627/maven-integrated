package mi.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import mi.dao.ICrudRepository;
import mi.dto.BaseDTO;
import mi.enumtype.Status;
import mi.model.BaseModel;

import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:18:56 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 基础业务接口实现类 ]
 */
@Transactional
public abstract class BaseServiceImpl<Model extends BaseModel,DTO extends BaseDTO> implements IBaseService<Model, DTO>{
	
	@Autowired
	private ICrudRepository<Model,DTO> crudRepository;
	
	@Autowired
	protected MessageSource messageSource;

	public Integer insert(Model model){
		return crudRepository.save(model);
	}
	
	public Collection<Model> selectByPageable(DTO dto){
		return crudRepository.findByPageable(dto);
	}
	
	public Collection<Model> selectByCriteria(DTO dto,Set<Criterion> criterions){
		return crudRepository.findByCriteria(dto,criterions);
	}
	
	public Model findByDTO(DTO dto){
		return crudRepository.findByDTO(dto);
	}
	
	public void clean(Model model){
		List<Model> list = (List<Model>) crudRepository.findAll();
		for(Model mo : list){
			crudRepository.delete(mo);
		}
	}
	
	public void delete(Model model){
		crudRepository.delete(model);
	}
	
	@Override
	public Model find(Model model) {
		return crudRepository.find(model);
	}

	@Override
	public void edit(Model model) {
		crudRepository.edit(model);
	}
	
	public Collection<Model> select(Model model){
		return crudRepository.select(model);
	}

	@Override
	public Long count(DTO dto) {
		return crudRepository.count(dto);
	}

	@Override
	public void congeal(Model model) {
		Model _model = find(model);
		_model.setStatus(Status.CONGEAL);
		crudRepository.edit(_model);
	}

	@Override
	public void thaw(Model model) {
		Model _model = find(model);
		_model.setStatus(Status.THAW);
		crudRepository.edit(_model);
	}
	
	@Override
	public void deleteForStatus(Model model) {
		Model _model = find(model);
		_model.setStatus(Status.DELETE);
		crudRepository.edit(_model);
	}
	
}
