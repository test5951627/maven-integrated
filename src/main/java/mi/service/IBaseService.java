package mi.service;

import java.util.Collection;
import java.util.Set;

import mi.dto.BaseDTO;
import mi.model.BaseModel;

import org.hibernate.criterion.Criterion;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:23:36 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 基础业务接口 ]
 */
public interface IBaseService<Model extends BaseModel,DTO extends BaseDTO>{
	
	/**
	 * 插入一条实体信息
	 * @param model 实体
	 * @return 实体主键
	 */
	Integer insert(Model model);
	
	/**
	 * 根据分页查询实体列表信息
	 * @param dto DTO实体
	 * @return 实体信息列表
	 */
	Collection<Model> selectByPageable(DTO dto);
	
	/**
	 * 根据高级查询实体列表信息
	 * @param dto DTO实体
	 * @param criterions 高级查询属性
	 * @return 实体信息列表
	 */
	Collection<Model> selectByCriteria(DTO dto,Set<Criterion> criterions);
	
	/**
	 * 根据DTO查询实体信息
	 * @param dto 实体
	 * @return 实体信息
	 */
	Model findByDTO(DTO dto);
	
	/**
	 * 清空当前实体的所有记录
	 * @param model 实体
	 */
	void clean(Model model);
	
	/**
	 * 根据实体删除实体信息
	 * @param model 实体
	 */
	void delete(Model model);
	
	/**
	 * 修改实体类信息
	 * @param model 实体
	 */
	void edit(Model model);
	
	/**
	 * 根据DTO获取实体信息个数
	 * @param dto
	 * @return 个数
	 */
	Long count(DTO dto);
	
	/**
	 * 根据实体查询实体列表信息
	 * @param model 实体
	 * @return 实体列表信息
	 */
	Collection<Model> select(Model model);
	
	/**
	 * 根据实体查询实体信息
	 * @param model 实体
	 * @return
	 */
	Model find(Model model);
	
	/**
	 * 冻结实体数据
	 * @param model 实体
	 */
	void congeal(Model model);
	
	/**
	 * 解冻实体数据
	 * @param model 实体
	 */
	void thaw(Model model);
	
	/**
	 * 删除实体数据（只是修改状态）
	 * @param model 实体
	 */
	void deleteForStatus(Model model);
}
