package mi.dao.demo;

import mi.dao.ICrudRepository;
import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;

public interface IStudentDao extends ICrudRepository<Student, StudentDTO>{

}
