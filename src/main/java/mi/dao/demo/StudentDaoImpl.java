package mi.dao.demo;

import org.springframework.stereotype.Repository;

import mi.dao.CrudRepositoryImpl;
import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;

@Repository
public class StudentDaoImpl extends CrudRepositoryImpl<Student, StudentDTO> implements
		IStudentDao {

}
