package mi.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import mi.dto.BaseDTO;
import mi.model.BaseModel;

import org.hibernate.criterion.Criterion;
import org.springframework.data.repository.Repository;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:17:07 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ CRUD接口 ]
 */
public interface ICrudRepository<Model extends BaseModel,DTO extends BaseDTO> extends Repository<Model,Integer>{
	
	/**
	 * 保存实现类信息
	 * @param model 实体类
	 * @return 插入数据的ID
	 */
	Integer save(Model model); 

	/**
	 * 根据主键查询实体类信息
	 * @param primaryKey 主键ID
	 * @return 实体类
	 */
    Model findOne(Integer primaryKey); 
    
    /**
     * 查找所有实体类信息
     * @return 实体类信息列表信息
     */
    Iterable<Model> findAll();          

    /**
     * 根据DTO统计实体类个数
     * @param dto
     * @return 实体类个数
     */
    Long count(DTO dto);                   

    /**
     * 根据实体类信息删除实体信息
     * @param model 实体类
     */
    void delete(Model model);       
    
    /**
     * 根据分页查询实体类信息列表
     * @param dto 分页DTO
     * @return 实体类信息分页列表
     */
    Collection<Model> findByPageable(DTO dto);
    
    /**
     * 根据Criteria查询条件查询列表
     * @param dto
     * @param criterions
     * @return 实体类信息列表
     */
    Collection<Model> findByCriteria(DTO dto,Set<Criterion> criterions);
    
    /**
     * 根据DTO查询实体类信息
     * @param dto 实体类
     * @return 实体类
     */
    Model findByDTO(DTO dto);

    /**
     * 根据主键判断实体类是否存在 
     * @param primaryKey 主键
     * @return true|false
     */
    boolean exists(Integer primaryKey);
    
    /**
     * 修改实体类信息
     * @param model 实体类
     */
    void edit(Model model);
    
    /**
     * 查询实体类信息列表
     * @param model
     * @return
     */
    List<Model> select(Model model);
    
    /**
     * 根据实体的Class获取在数据库中对应的表名
     * @param entityClass 实体类Class 
     * @return 实体类列表
     */
    String getEntityTableName(Class<Model> entityClass);
    
    /**
     * 根据实体类查询实体类
     * @param model 实体类
     * @return 实体类
     */
    Model find(Model model);

}
