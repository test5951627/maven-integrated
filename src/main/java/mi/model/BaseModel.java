package mi.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import mi.enumtype.Status;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:18:18 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ Model基类 ]
 */
public class BaseModel implements Serializable{
	
	public static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat DATETIME = new SimpleDateFormat("yyyy-MM-dd");

	private static final long serialVersionUID = 6190715959705710245L;

	public Integer id;//公用ID
	public Status status=Status.NORMAL;//公用状态
	public Date createTime;//公用创建时间
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public void setStatus(int status) {
		switch(status){
			case 0:
				this.status = Status.NORMAL;break;
			case 1:
				this.status = Status.CONGEAL;break;
			case 2:
				this.status = Status.THAW;break;
			case 3:
				this.status = Status.DELETE;break;
		}
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateTimeT(){
		if(null!=createTime){
			return TIMESTAMP.format(createTime);
		}
		return "error";
	}
	
	public String getCreateTimeD(){
		if(null!=createTime){
			return DATETIME.format(createTime);
		}
		return "error";
	}
	
}
