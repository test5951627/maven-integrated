package mi.web;

import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import mi.dto.BaseDTO;
import mi.model.BaseModel;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:24:57 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 基础Action接口 ]
 */
public interface IBaseAction<Model extends BaseModel,DTO extends BaseDTO> {
	
	public static final String SUCCESS="success";
	public static final String FAILURE="failure";
	
	@RequestMapping(value="content.html",method=RequestMethod.GET)
	public String content();
	
	@RequestMapping(value="edit.action",method=RequestMethod.POST)
	@ResponseBody
	public Object edit(@Valid Model model,BindingResult result);
	
	@RequestMapping(value="find.action",method=RequestMethod.POST)
	@ResponseBody
	public Model find(DTO dto);
	
	public Long getRecordCount(DTO dto);
	
	@RequestMapping(value="delete.action",method=RequestMethod.POST)
	@ResponseBody
	public Object delete(@Valid Model model,BindingResult result);
	
	@RequestMapping(value="add.action",method=RequestMethod.POST)
	@ResponseBody
	public Object add(@Valid Model model,BindingResult result);
	
	@RequestMapping(value="congeal.action",method=RequestMethod.POST)
	@ResponseBody
	public Object congeal(@Valid Model model,BindingResult result);
	
	@RequestMapping(value="thaw.action",method=RequestMethod.POST)
	@ResponseBody
	public Object thaw(@Valid Model model,BindingResult result);
	
	@RequestMapping(value="all.action",method=RequestMethod.POST)
	@ResponseBody
	public Collection<Model> all(DTO dto);
	
	@RequestMapping(value="maps.action",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> maps(final DTO dto);
	
}
