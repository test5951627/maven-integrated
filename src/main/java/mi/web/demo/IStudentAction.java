package mi.web.demo;

import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;
import mi.web.IBaseAction;

public interface IStudentAction extends IBaseAction<Student, StudentDTO> {

}
