**Maven 自动化集成测试**

1. 参考国外文章来编写的
2. 可测试 Spring+SpringMVC+Hibernate 技术
3. 可使用 HttpClient 测试 SpringMVC 中 Action
4. 增加统一验证功能
5. 数据可使用 hibernate 的自动导入数据功能


### 以下是部分测试代码


![测试插入数据](http://git.oschina.net/uploads/images/2015/0526/151814_ddd35faa_104479.png "测试插入数据")

![测试get](http://git.oschina.net/uploads/images/2015/0526/145741_58e79602_104479.png "get测试")

![测试post](http://git.oschina.net/uploads/images/2015/0526/150954_e88b5cfd_104479.png "post测试")

![eclipse测试](http://git.oschina.net/uploads/images/2015/0526/150628_b41163d5_104479.png "eclipse测试")

![项目结构](http://git.oschina.net/uploads/images/2015/0526/151200_ba49843c_104479.png "项目结构")

注意：使用eclipse执行`integration-test`，如果出现控制台乱码，请进行修改配置（`Window -> General -> Workspace -> Text file encoding 把Default(GBK) 修改成UTF-8`）

运行方法：`maven clean integration-test`

技术讨论：`174481989`
 